
import os
import sys
import yaml
import json
import requests
from dictor import dictor
from loguru import logger
import datetime
from app.views.common import get_config
from app.views.cmd import get_client

def get_api_token():
    creds = get_config('salt_api')
    user = dictor(creds, 'user', checknone=True)
    pw = dictor(creds, 'pw', checknone=True)
    host = dictor(creds, 'host', checknone=True)
    port = dictor(creds, 'port', checknone=True)

    url = 'https://' + host + ':' + str(port) + '/login'
    session = requests.Session()
    session.verify = False
    resp = session.post(url, json={'username': user, 'password': pw, 'eauth': 'pam'})
    data = resp.json()
    token = dictor(data, 'return.0.token', checknone=True)
    return token