from loguru import logger
from quart import render_template, redirect, url_for, flash, request, session
from werkzeug.urls import url_parse
from flask_login import login_user, login_required, logout_user, login_manager, current_user
from app import app
from app.models import User
from app.forms import LoginForm, NewUserForm, ChangePWForm
from app.views.db.user import _add_user, query_users, remove_user, _change_pw
from app.views.main import admin_only


@app.route('/login', methods=['GET', 'POST'])
async def login():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            await flash('Invalid username or password')
            return redirect(url_for('login'))
        login_user(user, remember=form.remember_me.data)
        return redirect(url_for('index'))
    return await render_template('login.html', title='Sign In', form=form)
    
@app.route('/logout')
def logout():
    logout_user()
    flash('You were logged out.')
    return redirect(url_for('index'))


@app.route('/show_users', methods=['GET'])
@login_required
@admin_only
async def show_users():
    return await render_template('users.html', all_users=query_users(), action='show_users', breadcrumb="User Management")


@app.route('/add_user', methods=['GET', 'POST'])
@login_required
@admin_only
async def add_user():
    breadcrumb = "User Management > Add New User"
    form = NewUserForm()
    if form.validate_on_submit():
        if not form.password1.data == form.password2.data:
            await flash('Passwords do not match.')
            return await render_template('users.html', all_users=query_users(), action='add_user', breadcrumb=breadcrumb, form=form)

        user_exists = User.query.filter_by(username=form.username.data).first()
        email_exists = User.query.filter_by(email=form.email.data).first()

        if user_exists or email_exists:
            if user_exists: msg = 'username is already taken'
            if email_exists: msg = 'email is already taken'
            await flash(msg)
            return await render_template('users.html', all_users=query_users(), action='add_user', breadcrumb=breadcrumb, form=form)

        _add_user(form.username.data, form.password1.data, form.email.data, form.admin.data)
        await flash(f'New user {form.username.data} created.')
        return redirect(url_for('show_users'))


    return await render_template('users.html', action='add_user', breadcrumb=breadcrumb, form=form)


@app.route('/delete_user/<username>', methods=['GET', 'POST'])
@login_required
@admin_only
async def delete_user(username):
    try:
        remove_user(username)
    except Exception as e:
        logger.error(str(e))
        await flash(str(e))
        return redirect(url_for('show_users'))

    await flash(f'user {username} has been deleted')
    return redirect(url_for('show_users'))

@app.route('/change_pw/<username>', methods=['GET', 'POST'])
@login_required
async def change_pw(username):

    if not current_user.username == username and not current_user.admin:
        return redirect(url_for('index'))
    
    breadcrumb = "User Management > Change Password"
    form = ChangePWForm()
    logger.debug(username)
    if form.validate_on_submit():
        if not form.password1.data == form.password2.data:
            await flash('Passwords do not match.')
            return await render_template('users.html', all_users=query_users(), action='change_pw', breadcrumb=breadcrumb, form=form, username=username)
        
        _change_pw(username, form.password1.data)
        

        # if user is changing their own password, log them out after update
        if current_user.username == username:
            await flash(f'Password for {username} has been updated, please re-login')
            return redirect(url_for('logout'))
        else:
            await flash(f'Password for {username} has been updated')
            return redirect(url_for('show_users'))

    return await render_template('users.html', action='change_pw', breadcrumb=breadcrumb, form=form, username=username)