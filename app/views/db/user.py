import sqlite3
import os
from loguru import logger
from werkzeug.security import generate_password_hash, check_password_hash
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Date, Integer, String, Boolean
from sqlalchemy import create_engine, ForeignKey
#from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm import sessionmaker
from config import BASEDIR
from app.models import User
from app import db

db_users = BASEDIR + '/app/views/db/users.db'

engine = create_engine('sqlite:///{}'.format(db_users), echo=True)
Base = declarative_base()

def query_users():
    ''' returns all users from User DB '''    
    users = User.query.all()
    return users

def _add_user(username, password, email, access):
    '''
    populate User DB with Silica regular users
    '''
    pw_hash = generate_password_hash(password)
    user = User(username, email, pw_hash, access)
    db.session.add(user)
    logger.info(f'inserting user {username} into users.db')

    try:
        # commit the record the database
        db.session.commit()
    except sqlite3.IntegrityError as e:
        logger.error(e)

def remove_user(username):
    ''' removes a single user '''
    # create a Session
    User.query.filter_by(username=username).delete()
    db.session.commit()

def _change_pw(username, password):
    ''' updates a user's password '''
    pw_hash = generate_password_hash(password)
    user = User.query.filter_by(username=username).first()
    user.password = pw_hash
    db.session.commit()
 