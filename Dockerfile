FROM centos:centos7
RUN yum install -y gcc openssl-devel bzip2-devel libffi libffi-devel && \
    yum install -y https://centos7.iuscommunity.org/ius-release.rpm && \
    yum update -y && \
    yum install -y python36u python36u-libs python36u-devel python36u-pip && \
	pip3.6 install pipenv  && \
	useradd -r -s /bin/false silica && \
	mkdir /opt/silica && \
	chmod 700 /opt/silica
ENV LC_ALL="en_GB.utf8"
ENV LANG="en_GB.utf8"
RUN pip3 install quart flask flask-bootstrap flask-login==0.4.1 flask-wtf flask-sqlalchemy  flask-apscheduler flask-testing sqlalchemy pyyaml dictor loguru requests tinydb pendulum hypercorn pytest autopep8  


COPY . /opt/silica
WORKDIR /opt/silica
#CMD /usr/local/bin/pipenv run silica
RUN python3  app/views/db/create_admin.py
CMD hypercorn app:app -k asyncio -w 1 -b 0.0.0.0:5100 --reload


